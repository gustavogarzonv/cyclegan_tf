import tensorflow as tf
import numpy as np
import os
import sys
import matplotlib.image as mpimg
import matplotlib.pyplot as plt

def _bytes_feature(value):
    """Returns a bytes_list from a string / byte."""
    if isinstance(value, type(tf.constant(0))):  # if value ist tensor
        value = value.numpy()  # get value of tensor
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


def _float_feature(value):
    """Returns a floast_list from a float / double."""
    return tf.train.Feature(float_list=tf.train.FloatList(value=[value]))


def _int64_feature(value):
    """Returns an int64_list from a bool / enum / int / uint."""
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))


def serialize_array(array):
    array = tf.io.serialize_tensor(array)
    return array


def parse_image(image, label):
    #define the structure of the data
    data = {
        'height': _int64_feature(image.shape[0]),
        'width': _int64_feature(image.shape[1]),
        'depth': _int64_feature(image.shape[2]),
        'raw_image': _bytes_feature(serialize_array(image)),
        'label': _int64_feature(label)
    }

    #create an Example, wrapping the single features
    out = tf.train.Example(features=tf.train.Features(feature=data))

    return out



def write_images_to_tensorflow_record(images, labels, filename: str = "images"):
    filename = filename + ".tfrecords"
    #create a writer
    writer = tf.io.TFRecordWriter(filename)
    count = 0
    #iterate over the data
    for index in range(len(images)):
        #get the data we want to write
        current_image = images[index]
        current_label = labels[index]

        #prepare the data
        out = parse_image(image=current_image, label=current_label)
        #write the example to the file
        writer.write(out.SerializeToString())
        count += 1
    #close the writer
    writer.close()
    print(f"Wrote {count} elements to TFRecord")



def parse_tfr_element(element):
    #use the same structure as before
    data = {
        'height': tf.io.FixedLenFeature([], tf.int64),
        'width': tf.io.FixedLenFeature([], tf.int64),
        'label': tf.io.FixedLenFeature([], tf.int64),
        'raw_image': tf.io.FixedLenFeature([], tf.string),
        'depth': tf.io.FixedLenFeature([], tf.int64),
    }

    #parse the data
    content = tf.io.parse_single_example(element, data)

    #get the values
    height = content['height']
    width = content['width']
    depth = content['depth']
    label = content['label']
    raw_image = content['raw_image']

    #reshape the image
    image = tf.io.parse_tensor(raw_image, out_type=tf.int16)
    image = tf.reshape(image, shape=[height, width, depth])
    return image, label


def get_dataset(filename):
    #create a dataset from the file
    dataset = tf.data.TFRecordDataset(filename)

    #map the parsing function to the dataset
    dataset = dataset.map(
        parse_tfr_element
    )

    return dataset


image_shape = (256, 256, 3)
number_of_images = 1000

#image_dataset = np.random.randint(low=0, high=256, size=(number_of_images, *image_shape), dtype=np.int16)
#labels = np.random.randint(low=0, high=number_of_classes, size=(number_of_images,))

pathadc = "data/custom-ncct2adc/trainB/"
pathncct = "data/custom-ncct2adc/trainA/"

mylist_adc = os.listdir(pathadc)
mylist_ncct = os.listdir(pathncct)
mylist_ncct.sort()
total = len(mylist_ncct)
k = 1

image_dataset = []

for fn in mylist_ncct:
    print(str(k)+" of "+str(total))
    if k==15:
        image_ncct = mpimg.imread(os.path.join(pathncct, fn))[...,:3]
        image_adc = mpimg.imread(os.path.join(pathadc, fn.replace("ncct", "adc")))[...,:3]
        print(fn)
        #print("GGARZON", image_ncct.shape)
        #print("GGARZON", image_adc.shape)
        image_dataset.append(np.hstack((image_ncct, image_adc)))
        plt.imshow(np.hstack((image_ncct, image_adc)), cmap="gray")
        plt.imsave("prueba.png", np.hstack((image_ncct, image_adc)))
        break
    k += 1

image_dataset = np.array(image_dataset)
print(image_dataset.shape)












#retrieved_dataset = get_dataset("/data/Datasets/tfrecords/apis_train.tfrecord")

#for sample in retrieved_dataset.take(1):
#    print(sample[0].shape)
#    print(sample[1].shape)