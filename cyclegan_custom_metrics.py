import os
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]="0"

from keras.layers import Layer, Input, Dropout, Conv2D, Activation, add, UpSampling2D, Conv2DTranspose, Flatten
from keras_contrib.layers.normalization.instancenormalization import InstanceNormalization, InputSpec
from keras.layers.advanced_activations import LeakyReLU
from keras.layers.core import Dense
from keras.optimizers import Adam
from keras.models import Model
from keras.engine.topology import Network
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from progress.bar import Bar
import datetime
import time
import json
import csv
import sys
import os
import keras.backend as K
import tensorflow as tf
from skimage.metrics import structural_similarity as ssim
from skimage.metrics import peak_signal_noise_ratio as psnr

from helper_funcs3 import *

image_folder = 'v2-noss-adc2dwi'

data = load_data(subfolder=image_folder)

opt = {}

# Data
opt['channels'] = data["nr_of_channels"]
opt['img_shape'] = data["image_size"] + (opt['channels'],)
opt['A_train'] = data["trainA_images"]
opt['B_train'] = data["trainB_images"]
opt['A_test'] = data["testA_images"]
opt['B_test'] = data["testB_images"]
opt['testA_image_names'] = data["testA_image_names"]
opt['testB_image_names'] = data["testB_image_names"]
opt['trainA_image_names'] = data["trainA_image_names"]
opt['trainB_image_names'] = data["trainB_image_names"]
print(opt['A_train'].shape)
print(opt['B_train'].shape)

opt['paired_data'] = False

# Training parameters
opt['lambda_ABA'] = 10.0  # Cyclic loss weight A_2_B
opt['lambda_BAB'] = 10.0  # Cyclic loss weight B_2_A
opt['lambda_adversarial'] = 1.0  # Weight for loss from discriminator guess on synthetic images
opt['learning_rate_D'] = 2e-4
opt['learning_rate_G'] = 2e-4
opt['generator_iterations'] = 3  # Number of generator training iterations in each training loop
opt['discriminator_iterations'] = 1  # Number of discriminator training iterations in each training loop
opt['synthetic_pool_size'] = 50  # Size of image pools used for training the discriminators
opt['beta_1'] = 0.5  # Adam parameter
opt['beta_2'] = 0.999  # Adam parameter
opt['batch_size'] = 16  # Number of images per batch
opt['epochs'] = 100  # Choose multiples of 20 since the models are saved each 20th epoch

# Output parameters
opt['save_models'] = True  # Save or not the generator and discriminator models
opt['save_training_img'] = True  # Save or not example training results or only tmp.png
opt['save_training_img_interval'] = 1  # Number of epoch between saves of intermediate training results
opt['self.tmp_img_update_frequency'] = 3  # Number of batches between updates of tmp.png

# Architecture parameters
opt['use_instance_normalization'] = True  # Use instance normalization or batch normalization
opt['use_dropout'] = False  # Dropout in residual blocks
opt['use_bias'] = True  # Use bias
opt['use_linear_decay'] = True  # Linear decay of learning rate, for both discriminators and generators
opt['decay_epoch'] = 100  # The epoch where the linear decay of the learning rates start
opt['use_patchgan'] = True  # PatchGAN - if false the discriminator learning rate should be decreased
opt['use_resize_convolution'] = False  # Resize convolution - instead of transpose convolution in deconvolution layers (uk) - can reduce checkerboard artifacts but the blurring might affect the cycle-consistency
opt['discriminator_sigmoid'] = True  # Add a final sigmoid activation to the discriminator

# Tweaks
opt['REAL_LABEL'] = 1.0  # Use e.g. 0.9 to avoid training the discriminators to zero loss

#GGARZON
opt['best_ssim'] = 0

# Discriminator layers
def ck(model, opt, x, k, use_normalization, use_bias):
    x = Conv2D(filters=k, kernel_size=4, strides=2, padding='same', use_bias=use_bias)(x)
    if use_normalization:
        x = model['normalization'](axis=3, center=True, epsilon=1e-5)(x, training=True)
    x = LeakyReLU(alpha=0.2)(x)
    return x

# First generator layer
def c7Ak(model, opt, x, k):
    x = Conv2D(filters=k, kernel_size=7, strides=1, padding='valid', use_bias=opt['use_bias'])(x)
    x = model['normalization'](axis=3, center=True, epsilon=1e-5)(x, training=True)
    x = Activation('relu')(x)
    return x

# Downsampling
def dk(model, opt, x, k):  # Should have reflection padding
    x = Conv2D(filters=k, kernel_size=3, strides=2, padding='same', use_bias=opt['use_bias'])(x)
    x = model['normalization'](axis=3, center=True, epsilon=1e-5)(x, training=True)
    x = Activation('relu')(x)
    return x

# Residual block
def Rk(model, opt, x0):
    k = int(x0.shape[-1])

    # First layer
    x = ReflectionPadding2D((1,1))(x0)
    x = Conv2D(filters=k, kernel_size=3, strides=1, padding='valid', use_bias=opt['use_bias'])(x)
    x = model['normalization'](axis=3, center=True, epsilon=1e-5)(x, training=True)
    x = Activation('relu')(x)

    if opt['use_dropout']:
        x = Dropout(0.5)(x)

    # Second layer
    x = ReflectionPadding2D((1, 1))(x)
    x = Conv2D(filters=k, kernel_size=3, strides=1, padding='valid', use_bias=opt['use_bias'])(x)
    x = model['normalization'](axis=3, center=True, epsilon=1e-5)(x, training=True)
    # Merge
    x = add([x, x0])

    return x

# Upsampling
def uk(model, opt, x, k):
    # (up sampling followed by 1x1 convolution <=> fractional-strided 1/2)
    if opt['use_resize_convolution']:
        x = UpSampling2D(size=(2, 2))(x)  # Nearest neighbor upsampling
        x = ReflectionPadding2D((1, 1))(x)
        x = Conv2D(filters=k, kernel_size=3, strides=1, padding='valid', use_bias=opt['use_bias'])(x)
    else:
        x = Conv2DTranspose(filters=k, kernel_size=3, strides=2, padding='same', use_bias=opt['use_bias'])(x)  # this matches fractionally stided with stride 1/2
    x = model['normalization'](axis=3, center=True, epsilon=1e-5)(x, training=True)
    x = Activation('relu')(x)
    return x


def build_discriminator(model, opt, name=None):
    # Input
    input_img = Input(shape=opt['img_shape'])

    # Layers 1-4
    x = ck(model, opt, input_img, 64, False, True) #  Instance normalization is not used for this layer)
    x = ck(model, opt, x, 128, True, opt['use_bias'])
    x = ck(model, opt, x, 256, True, opt['use_bias'])
    x = ck(model, opt, x, 512, True, opt['use_bias'])

    # Layer 5: Output
    if opt['use_patchgan']:
        x = Conv2D(filters=1, kernel_size=4, strides=1, padding='same', use_bias=True)(x)
    else:
        x = Flatten()(x)
        x = Dense(1)(x)

    if opt['discriminator_sigmoid']:
        x = Activation('sigmoid')(x)

    return Model(inputs=input_img, outputs=x, name=name)

def build_generator(model, opt, name=None):
    # Layer 1: Input
    input_img = Input(shape=opt['img_shape'])
    x = ReflectionPadding2D((3, 3))(input_img)
    x = c7Ak(model, opt, x, 32)

    # Layer 2-3: Downsampling
    x = dk(model, opt, x, 64)
    x = dk(model, opt, x, 128)

    # Layers 4-12: Residual blocks
    for _ in range(4, 13):
        x = Rk(model, opt, x)

    # Layer 13:14: Upsampling
    x = uk(model, opt, x, 64)
    x = uk(model, opt, x, 32)

    # Layer 15: Output
    x = ReflectionPadding2D((3, 3))(x)
    x = Conv2D(opt['channels'], kernel_size=7, strides=1, padding='valid', use_bias=True)(x)
    x = Activation('tanh')(x)

    return Model(inputs=input_img, outputs=x, name=name)


#@title Generator
from keras import layers

def u_block(input_tensor, 
            encoder_num_filters=[64, 128, 256, 512, 512, 512, 512, 512],
            decoder_num_filters=[512, 1024, 1024, 1024, 1024, 512, 256, 128],
            use_instance_norm=True,
            block_prefix=''):
    
    x = input_tensor
    encoder_blocks_outputs = []
    
    # Encoder path.
    for i, num_filters in enumerate(encoder_num_filters):
        x = layers.Conv2D(num_filters, 
                          kernel_size=4, 
                          strides=2, 
                          padding='same', 
                          name=f'{block_prefix}_EncoderBlock{i+1}-Conv')(x)
        if use_instance_norm:
            x = InstanceNormalization(name=f'{block_prefix}_EncoderBlock{i+1}-Instancenorm')(x)
        else:
            x = layers.BatchNormalization(name=f'{block_prefix}_EncoderBlock{i+1}-Batchnorm')(x)
        x = layers.Activation('relu', name=f'{block_prefix}_EncoderBlock{i+1}-ReLU')(x)
        
        # Append the encoder blocks outputs, except for the last one.
        if i != len(encoder_num_filters) - 1: 
            encoder_blocks_outputs.append(x)

    # Decoder path.
    for i, num_filters in enumerate(decoder_num_filters):
        x = layers.Conv2DTranspose(
            num_filters, 
            kernel_size=(4, 4), 
            strides=(2, 2), 
            padding='same',
            name=f'{block_prefix}_DecoderBlock{i+1}-TransposedConv'
        )(x)
        x = layers.Activation('relu', name=f'{block_prefix}_DecoderBlock{i+1}-ReLU')(x)

        # All the decoder blocks have concatenate the encoder block output with
        # the same spatial dimentions except for the last one.
        if i != len(decoder_num_filters) - 1: 
            x = layers.Concatenate(
                name=f'{block_prefix}_DecoderBlock{i+1}-Concat'
            )([encoder_blocks_outputs[-(i + 1)], x])

    # Last operation: 1x1 conv to map the image to the input n_channels.
    x = layers.Conv2D(1,   #input_tensor.shape[-1]
                      kernel_size=(1, 1), 
                      name=f'{block_prefix}_Decoder-Conv1x1')(x)
    output = layers.Activation('tanh', name=f'{block_prefix}_Decoder-Tanh')(x)

    return output

def build_casnet_generator(input_shape=(256, 256, 1), 
                           n_blocks=2, 
                           use_instance_norm=True,
                           name=None):
    """Create a CasNet Generator using UBlocks"""
    input_tensor = Input(shape=input_shape)
    x = input_tensor
    for i in range(n_blocks):
        x = u_block(x, block_prefix=f'UBlock{i+1}', use_instance_norm=use_instance_norm)
    
    return Model(inputs=input_tensor, outputs=x, name=name)


# Mean squared error
def mse(y_true, y_pred):
    loss = tf.reduce_mean(tf.squared_difference(y_pred, y_true))
    return loss

# Mean absolute error
def mae(y_true, y_pred):
    loss = tf.reduce_mean(tf.abs(y_pred - y_true))
    return loss


model = {}

# Normalization
model['normalization'] = InstanceNormalization

# Optimizers
model['opt_D'] = Adam(opt['learning_rate_D'], opt['beta_1'], opt['beta_2'])
model['opt_G'] = Adam(opt['learning_rate_G'], opt['beta_1'], opt['beta_2'])

# Build discriminators
D_A = build_discriminator(model, opt, name='D_A')
D_B = build_discriminator(model, opt, name='D_B')

# Define discriminator models
image_A = Input(shape=opt['img_shape'])
image_B = Input(shape=opt['img_shape'])
guess_A = D_A(image_A)
guess_B = D_B(image_B)
model['D_A'] = Model(inputs=image_A, outputs=guess_A, name='D_A_model')
model['D_B'] = Model(inputs=image_B, outputs=guess_B, name='D_B_model')

# Compile discriminator models
loss_weights_D = [0.5]  # 0.5 since we train on real and synthetic images
model['D_A'].compile(optimizer=model['opt_D'],
                 loss=mse,
                 loss_weights=loss_weights_D)
model['D_B'].compile(optimizer=model['opt_D'],
                 loss=mse,
                 loss_weights=loss_weights_D)

# Use containers to make a static copy of discriminators, used when training the generators
model['D_A_static'] = Network(inputs=image_A, outputs=guess_A, name='D_A_static_model')
model['D_B_static'] = Network(inputs=image_B, outputs=guess_B, name='D_B_static_model')

# Do not update discriminator weights during generator training
model['D_A_static'].trainable = False
model['D_B_static'].trainable = False

# Build generators
#model['G_A2B'] = build_generator(model, opt, name='G_A2B_model')
#model['G_B2A'] = build_generator(model, opt, name='G_B2A_model')
model['G_A2B'] = build_casnet_generator(input_shape=(256,256,1), n_blocks=1, name='G_A2B_model')
model['G_B2A'] = build_casnet_generator(input_shape=(256,256,1), n_blocks=1, name='G_B2A_model')

# Define full CycleGAN model, used for training the generators
real_A = Input(shape=opt['img_shape'], name='real_A')
real_B = Input(shape=opt['img_shape'], name='real_B')
synthetic_B = model['G_A2B'](real_A)
synthetic_A = model['G_B2A'](real_B)
dB_guess_synthetic = model['D_B_static'](synthetic_B)
dA_guess_synthetic = model['D_A_static'](synthetic_A)
reconstructed_A = model['G_B2A'](synthetic_B)
reconstructed_B = model['G_A2B'](synthetic_A)

# Compile full CycleGAN model
model_outputs = [reconstructed_A, reconstructed_B,
                 dB_guess_synthetic, dA_guess_synthetic]
compile_losses = [mae, mae,
                  mse, mse]
compile_weights = [opt['lambda_ABA'], opt['lambda_BAB'],
                   opt['lambda_adversarial'], opt['lambda_adversarial']]

model['G_model'] = Model(inputs=[real_A, real_B],
                     outputs=model_outputs,
                     name='G_model')

model['G_model'].compile(optimizer=model['opt_G'],
                     loss=compile_losses,
                     loss_weights=compile_weights)


# opt['date_time'] = time.strftime('%Y%m%d-%H%M%S', time.localtime()) + '-' + image_folder
# # Output folder for run data and images
# opt['out_dir'] = os.path.join('images', opt['date_time'])
# if not os.path.exists(opt['out_dir']):
#     os.makedirs(opt['out_dir'])
# # Output folder for saved models
# if opt['save_models']:
#     opt['model_out_dir'] = os.path.join('saved_models', opt['date_time'])
#     if not os.path.exists(opt['model_out_dir']):
#         os.makedirs(opt['model_out_dir'])
# write_metadata_to_JSON(model, opt)
# # Don't pre-allocate GPU memory; allocate as-needed
# config = tf.ConfigProto()
# config.gpu_options.allow_growth = True
# K.tensorflow_backend.set_session(tf.Session(config=config))


# def train(model, opt):

#     def run_training_batch():

#         # ======= Discriminator training ======
#         # Generate batch of synthetic images
#         synthetic_images_B = model['G_A2B'].predict(real_images_A)
#         synthetic_images_A = model['G_B2A'].predict(real_images_B)
#         synthetic_images_B = synthetic_pool_B.query(synthetic_images_B)
#         synthetic_images_A = synthetic_pool_A.query(synthetic_images_A)
        
#         # Train discriminators on batch
#         D_loss = []
#         for _ in range(opt['discriminator_iterations']):
#             D_A_loss_real = model['D_A'].train_on_batch(x=real_images_A, y=ones)
#             D_B_loss_real = model['D_B'].train_on_batch(x=real_images_B, y=ones)
#             D_A_loss_synthetic = model['D_A'].train_on_batch(x=synthetic_images_A, y=zeros)
#             D_B_loss_synthetic = model['D_B'].train_on_batch(x=synthetic_images_B, y=zeros)
#             D_A_loss = D_A_loss_real + D_A_loss_synthetic
#             D_B_loss = D_B_loss_real + D_B_loss_synthetic
#             D_loss.append(D_A_loss + D_B_loss)

#         # ======= Generator training ==========
#         target_data = [real_images_A, real_images_B, ones, ones]  # Reconstructed images need to match originals, discriminators need to predict ones

#         # Train generators on batch
#         G_loss = []
#         for _ in range(opt['generator_iterations']):
#             G_loss.append(model['G_model'].train_on_batch(
#                 x=[real_images_A, real_images_B], y=target_data))

#         # =====================================

#         # Update learning rates
#         if opt['use_linear_decay'] and epoch >= opt['decay_epoch']:
#             update_lr(model['D_A'], decay_D)
#             update_lr(model['D_B'], decay_D)
#             update_lr(model['G_model'], decay_G)

#         # Store training losses
#         D_A_losses.append(D_A_loss)
#         D_B_losses.append(D_B_loss)
#         D_losses.append(D_loss[-1])

#         ABA_reconstruction_loss = G_loss[-1][1]
#         BAB_reconstruction_loss = G_loss[-1][2]
#         reconstruction_loss = ABA_reconstruction_loss + BAB_reconstruction_loss
#         G_AB_adversarial_loss = G_loss[-1][3]
#         G_BA_adversarial_loss = G_loss[-1][4]

#         ABA_reconstruction_losses.append(ABA_reconstruction_loss)
#         BAB_reconstruction_losses.append(BAB_reconstruction_loss)
#         reconstruction_losses.append(reconstruction_loss)
#         G_AB_adversarial_losses.append(G_AB_adversarial_loss)
#         G_BA_adversarial_losses.append(G_BA_adversarial_loss)
#         G_losses.append(G_loss[-1][0])

#         # Print training status
#         print('\n')
#         print('Epoch ---------------------', epoch, '/', opt['epochs'])
#         print('Loop index ----------------', loop_index + 1, '/', nr_im_per_epoch)
#         if opt['discriminator_iterations'] > 1:
#             print('  Discriminator losses:')
#             for i in range(opt['discriminator_iterations']):
#                 print('D_loss', D_loss[i])
#         if opt['generator_iterations'] > 1:
#             print('  Generator losses:')
#             for i in range(opt['generator_iterations']):
#                 print('G_loss', G_loss[i])
#         print('  Summary:')
#         print('D_lr:', K.get_value(model['D_A'].optimizer.lr))
#         print('G_lr', K.get_value(model['G_model'].optimizer.lr))
#         print('D_loss: ', D_loss[-1])
#         print('G_loss: ', G_loss[-1][0])
#         print('reconstruction_loss: ', reconstruction_loss)
#         print_ETA(opt, start_time, epoch, nr_im_per_epoch, loop_index)
#         sys.stdout.flush()

#         if loop_index % 3*opt['batch_size'] == 0:
#             # Save temporary images continously
#             save_tmp_images(model, opt, real_images_A[0], real_images_B[0],
#                                  synthetic_images_A[0], synthetic_images_B[0])
        
#         #GGARZON
#         mysynthetic_image = model['G_B2A'].predict(myrealimageB)
#         current_ssim = ssim(myrealimageA[0,:,:,0], mysynthetic_image[0,:,:,0])

#         if current_ssim > opt['best_ssim']:
#             #save_model(opt, model['D_A'], epoch)
#             #save_model(opt, model['D_B'], epoch)
#             save_model(opt, model['G_A2B'], epoch)
#             save_model(opt, model['G_B2A'], epoch)
#             opt['best_ssim'] = current_ssim
#             print("GGARZON: new best SSIM (", str(opt['best_ssim']) ,") at epoch ", str(epoch), " and iteration ", str(loop_index+1))


#     # ======================================================================
#     # Begin training
#     # ======================================================================

#     imgpath = "data/"+image_folder+"/testA/test_022_9.png"
#     image = mpimg.imread(imgpath)
#     image = image * 2 - 1
#     image = image[:,:,0]
#     myrealimageA = image[np.newaxis,:,:,np.newaxis]

#     imgpath = "data/"+image_folder+"/testB/test_022_9.png"
#     image = mpimg.imread(imgpath)
#     image = image * 2 - 1
#     image = image[:,:,0]
#     myrealimageB = image[np.newaxis,:,:,np.newaxis]

#     if opt['save_training_img'] and not os.path.exists(os.path.join(opt['out_dir'], 'train_A')):
#         os.makedirs(os.path.join(opt['out_dir'], 'train_A'))
#         os.makedirs(os.path.join(opt['out_dir'], 'train_B'))
#         os.makedirs(os.path.join(opt['out_dir'], 'test_A'))
#         os.makedirs(os.path.join(opt['out_dir'], 'test_B'))

#     D_A_losses = []
#     D_B_losses = []
#     D_losses = []

#     ABA_reconstruction_losses = []
#     BAB_reconstruction_losses = []
#     reconstruction_losses = []
#     G_AB_adversarial_losses = []
#     G_BA_adversarial_losses = []
#     G_losses = []

#     # Image pools used to update the discriminators
#     synthetic_pool_A = ImagePool(opt['synthetic_pool_size'])
#     synthetic_pool_B = ImagePool(opt['synthetic_pool_size'])

#     # Labels used for discriminator training
#     label_shape = (opt['batch_size'],) + model['D_A'].output_shape[1:]
#     ones = np.ones(shape=label_shape) * opt['REAL_LABEL']
#     zeros = ones * 0

#     # Linear learning rate decay
#     if opt['use_linear_decay']:
#         decay_D, decay_G = get_lr_linear_decay_rate(opt)

#     nr_train_im_A = opt['A_train'].shape[0]
#     nr_train_im_B = opt['B_train'].shape[0]
#     nr_im_per_epoch = int(np.ceil(np.max((nr_train_im_A, nr_train_im_B)) / opt['batch_size']) * opt['batch_size'])

#     # Start stopwatch for ETAs
#     start_time = time.time()
#     timer_started = False

#     for epoch in range(1, opt['epochs'] + 1):
#         # random_order_A = np.random.randint(nr_train_im_A, size=nr_im_per_epoch)
#         # random_order_B = np.random.randint(nr_train_im_B, size=nr_im_per_epoch)

#         random_order_A = np.concatenate((np.random.permutation(nr_train_im_A),
#                                          np.random.randint(nr_train_im_A, size=nr_im_per_epoch - nr_train_im_A)))
#         random_order_B = np.concatenate((np.random.permutation(nr_train_im_B),
#                                          np.random.randint(nr_train_im_B, size=nr_im_per_epoch - nr_train_im_B)))

#         # Train on image batch
#         for loop_index in range(0, nr_im_per_epoch, opt['batch_size']):
#             indices_A = random_order_A[loop_index:loop_index + opt['batch_size']]
#             indices_B = random_order_B[loop_index:loop_index + opt['batch_size']]

#             real_images_A = opt['A_train'][indices_A]
#             real_images_B = opt['B_train'][indices_B]

#             # Train on image batch
#             run_training_batch()

#             # Start timer after first (slow) iteration has finished
#             if not timer_started:
#                 start_time = time.time()
#                 timer_started = True

#         # Save training images
#         if opt['save_training_img'] and epoch % opt['save_training_img_interval'] == 0:
#             print('\n', '\n', '-------------------------Saving images for epoch', epoch, '-------------------------', '\n', '\n')
#             save_epoch_images(model, opt, epoch)

#         # Save model
#         #if opt['save_models'] and epoch % 25 == 0:
#         #if opt['save_models'] and epoch % 2 == 0:
#         #    #save_model(opt, model['D_A'], epoch)
#         #    #save_model(opt, model['D_B'], epoch)
#         #    save_model(opt, model['G_A2B'], epoch)
#         #    save_model(opt, model['G_B2A'], epoch)

#         # Save training history
#         training_history = {
#             'DA_losses': D_A_losses,
#             'DB_losses': D_B_losses,
#             'G_AB_adversarial_losses': G_AB_adversarial_losses,
#             'G_BA_adversarial_losses': G_BA_adversarial_losses,
#             'ABA_reconstruction_losses': ABA_reconstruction_losses,
#             'BAB_reconstruction_losses': BAB_reconstruction_losses,
#             'reconstruction_losses': reconstruction_losses,
#             'D_losses': D_losses,
#             'G_losses': G_losses}
#         write_loss_data_to_file(opt, training_history)

#train(model, opt)






folder = "20230831-104831-v2-noss-adc2dwi" #ADC 2 DWI noss

#pre_path = "/data/gustavogarzon/"
pre_path = ""
checkpoint_path1 = pre_path+"saved_models/"+folder+"/G_A2B_model_weights_epoch.hdf5"
checkpoint_path2 = pre_path+"saved_models/"+folder+"/G_B2A_model_weights_epoch.hdf5"
model["G_A2B"].load_weights(checkpoint_path1)
model["G_B2A"].load_weights(checkpoint_path2)






def maskbox(imgpath):
    #imgpath = "data-old/customnoss/testmaskB/"+fn
    image = mpimg.imread(imgpath)
    h_start = 0
    h_end = 0
    v_start = 0
    v_end = 0
    for kk,p in enumerate(np.sum(image[:,:,0], axis=0)):
        if(p!=0):
            h_start = kk
            print(kk)
            break
    for p in range(255,0,-1):
        if(np.sum(image[:,:,0], axis=0)[p]!=0):
            h_end = p
            print(p)
            break
    for kk,p in enumerate(np.sum(image[:,:,0], axis=1)):
        if(p!=0):
            v_start = kk
            print(kk)
            break
    for p in range(255,0,-1):
        if(np.sum(image[:,:,0], axis=1)[p]!=0):
            v_end = p
            print(p)
            break
    return h_start,h_end,v_start,v_end



all_s1 = []
all_s2 = []
all_s3 = []
all_s4 = []
all_s5 = []
all_s6 = []
all_s7 = []
all_s8 = []

for k,f in enumerate(opt['testB_image_names']):
    print(k+1, "of", len(opt['testB_image_names']), end="\r")
    synthetic_images_A = model['G_B2A'].predict(opt['B_test'][k][np.newaxis,:])
    #reconstructed_image_B = model['G_A2B'].predict(synthetic_images_A)
    myrealimageA = mpimg.imread("data/"+image_folder+"/testA/"+f)
    ####image = mpimg.imread("data/"+image_folder+"/testA/"+pts[0]+"_"+pts[1]+"_"+pts[3])
    myrealimageA = myrealimageA[:,:,0]
    myrealimageA = (myrealimageA*2)-1
    s1 = ssim(myrealimageA, synthetic_images_A[0,:,:,0])
    s2 = psnr(myrealimageA, synthetic_images_A[0,:,:,0])
    all_s1.append(s1)
    all_s2.append(s2)
    #print("ssim:", s1)
    #print("psnr:", s2)
    # pts = f.split("_") #train_048_r1_mask_10.png
    # fn = pts[0]+"_"+pts[1]+"_r1_mask_"+pts[3]
    # #print(fn)
    # s3 = s4 = s5 = s6 = s7 = s8 = 0
    # h_start,h_end,v_start,v_end = maskbox(fn)
    # if h_start!=0 and h_end!=0 and v_start!=0 and v_end!=0:
    #     ws = None
    #     if opt['B_test'][k][h_start:h_end+1,v_start:v_end+1,0].shape[0]<7 or opt['B_test'][k][h_start:h_end+1,v_start:v_end+1,0].shape[1]<7:
    #         ws = 3
    #     s3 = ssim(opt['B_test'][k][h_start:h_end+1,v_start:v_end+1,0], reconstructed_image_B[0,h_start:h_end+1,v_start:v_end+1,0], win_size=ws)
    #     s4 = psnr(opt['B_test'][k][h_start:h_end+1,v_start:v_end+1,0], reconstructed_image_B[0,h_start:h_end+1,v_start:v_end+1,0])
    #     all_s3.append(s3)
    #     all_s4.append(s4)
    # plt.subplot(1,2,1)
    # plt.imshow(myrealimageA, cmap="gray")
    # plt.axis('off')
    # plt.subplot(1,2,2)
    # plt.imshow(synthetic_images_A[0,:,:,0], cmap="gray")
    # plt.axis('off')
    # plt.savefig("prueba_ssim.png", bbox_inches='tight')
    #plt.imsave("prueba_ssim.png", synthetic_images_A[0,:,:,0], cmap="gray")
    #break

#     print(f.replace("adc","ncct"))
#     image = mpimg.imread("data/"+image_folder+"/testA/"+f.replace("adc","ncct"))
#     ####image = mpimg.imread("data/"+image_folder+"/testA/"+pts[0]+"_"+pts[1]+"_"+pts[3])
#     image = image[:,:,0]
#     image = (image*2)-1
#     plt.figure(figsize=(12,4))
#     plt.subplot(1,3,1)
#     plt.imshow(opt['B_test'][k][:,:,0], cmap="gray")
#     plt.axis('off')
#     plt.subplot(1,3,2)
#     plt.imshow(synthetic_images_A[0,:,:,0], cmap="gray")
#     plt.axis('off')
#     plt.subplot(1,3,3)
#     plt.imshow(image, cmap="gray")
#     plt.axis('off')
#     #plt.subplot(1,4,4)
#     #plt.imshow(image, cmap="gray")
#     #plt.axis('off')
#     plt.show()
#     #print(np.min(image), np.max(image))
#     #print(np.min(synthetic_images_A), np.max(synthetic_images_A))
#     s5 = ssim(image, synthetic_images_A[0,:,:,0])
#     s6 = psnr(image, synthetic_images_A[0,:,:,0])
    
# #     plt.subplot(1,2,1)
# #     plt.imshow(image, cmap="gray")
# #     plt.subplot(1,2,2)
# #     plt.imshow(synthetic_images_A[0,:,:,0], cmap="gray")
    
#     all_s5.append(s5)
#     all_s6.append(s6)
    
#     h_start,h_end,v_start,v_end = maskbox(fn)
#     if h_start!=0 and h_end!=0 and v_start!=0 and v_end!=0:
#         ws = None
#         if opt['B_test'][k][h_start:h_end+1,v_start:v_end+1,0].shape[0]<7 or opt['B_test'][k][h_start:h_end+1,v_start:v_end+1,0].shape[1]<7:
#             ws = 3
#         s7 = ssim(image[h_start:h_end+1,v_start:v_end+1], synthetic_images_A[0,h_start:h_end+1,v_start:v_end+1,0], win_size=ws)
#         s8 = psnr(image[h_start:h_end+1,v_start:v_end+1], synthetic_images_A[0,h_start:h_end+1,v_start:v_end+1,0])
#         all_s7.append(s7)
#         all_s8.append(s8)
    
#     print(s1, s2, s3, s4, s5, s6, s7, s8)
#     #break

avg_s1 = np.mean(all_s1)
avg_s2 = np.mean(all_s2)
# avg_s3 = np.mean(all_s3)
# avg_s4 = np.mean(all_s4)
# avg_s5 = np.mean(all_s5)
# avg_s6 = np.mean(all_s6)
# avg_s7 = np.mean(all_s7)
# avg_s8 = np.mean(all_s8)

print("avg ssim:", avg_s1)
print("avg psnr:", avg_s2)